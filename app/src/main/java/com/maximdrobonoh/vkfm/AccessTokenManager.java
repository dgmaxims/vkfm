/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.maximdrobonoh.vkfm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.maximdrobonoh.vkfm.model.AccessToken;

/**
 * An AccessTokenManager needs for storing and
 * receiving access token from sharedPreference
 */
public class AccessTokenManager {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String EXPIRES_IN = "expires_in";
    public static final String USER_ID = "user_id";
    public static final String CREATED = "created";
    public static final String ACCESS_TOKEN_NULL = "access_token_null";

    private SharedPreferences preferences;

    public AccessTokenManager(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveAccessToken(AccessToken token) {
        SharedPreferences.Editor edit = preferences.edit();

        edit.putString(ACCESS_TOKEN, token.key);
        edit.putInt(EXPIRES_IN, token.expiresIn);
        edit.putString(USER_ID, token.userId);
        edit.apply();
    }

    public boolean isExpired() {
        int expiresIn = preferences.getInt(EXPIRES_IN, 0);
        int created = preferences.getInt(CREATED, 0);

        return expiresIn > 0 && expiresIn * 1000 + created < System.currentTimeMillis();
    }

    public Long getUserId() {
        return Long.parseLong(preferences.getString(USER_ID, "0"));
    }

    public String getAccessKey() {
        return preferences.getString(ACCESS_TOKEN, ACCESS_TOKEN_NULL);
    }
}
