package com.maximdrobonoh.vkfm;

import android.app.Application;
import android.content.Intent;

import com.maximdrobonoh.vkfm.database.DatabaseManager;
import com.maximdrobonoh.vkfm.player.PlayerService;

/**
 * Created by maximdrobonoh on 07.01.16.
 */
public class App extends Application {

    public static App sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        setImageLoader();
        sInstance = this;
        DatabaseManager.init(this);
    }

    private void setImageLoader() {
        startService(new Intent(this, PlayerService.class)
                .setAction("start"));
    }
}
