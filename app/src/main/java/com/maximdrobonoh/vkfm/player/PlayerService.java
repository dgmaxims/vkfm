package com.maximdrobonoh.vkfm.player;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Audio;

import java.io.IOException;

/**
 * Created by maximdrobonoh on 08.01.16.
 */
public class PlayerService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener {

    public static final int EXTRA_PLAY_PAUSE = 1;
    public static final String ACTION_AUDIO_CONTROLLER = "audio_controller";

    private static PlayerService playerSerivceInstance = null;
    private Audio sSong;

    NotificationManager mNotificationManager;
    State mState = State.Retrieving;

    private MediaPlayer mMediaPlayer = null;
    private int mBufferPosition;
    private Context mContext;

    public static PlayerService getInstance() {
        return playerSerivceInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        playerSerivceInstance = this;
        mContext = this;
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();

        if (action.equals(ACTION_AUDIO_CONTROLLER)) {
            int commandId = intent.getIntExtra(ACTION_AUDIO_CONTROLLER, EXTRA_PLAY_PAUSE);

            switch (commandId) {
                case EXTRA_PLAY_PAUSE: {
                    if (isPlaying()) {
                        pauseSong();
                    } else {
                        continueToPlay();
                    }
                }
            }
        }
        return START_NOT_STICKY;
    }

    public Audio getCurrentSong() {
        if ( sSong != null )
            return sSong;
        return new Audio();
    }

    public void toPlay() {
        if (mMediaPlayer != null) {
            mMediaPlayer.pause();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        initPlayer();
    }

    public void continueToPlay() {
        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {

            mMediaPlayer.start();
            mState = State.Playing;
            createNotification();
        }
    }

    public long getCurrentSongId() {
        if ( sSong != null )
            return sSong.getAudioId();
        return -1;
    }

    private void initPlayer() {
        try {
            mMediaPlayer.setDataSource(sSong.getUrl());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mMediaPlayer.prepareAsync();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        mState = State.Preparing;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        mState = State.Playing;
        createNotification();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        setmBufferPosition(percent * getSongDuration() / 100);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }
        mState = State.Retrieving;
    }

    public int getSongDuration() {
        return mMediaPlayer.getDuration();
    }

    public void pauseSong() {
        if (mState.equals(State.Playing)) {
            mMediaPlayer.pause();
            mState = State.Paused;

            createNotification();
        }
    }

    public void stopSong() {
        if (mState.equals(State.Playing)) {
            mMediaPlayer.stop();
        }
    }

    public boolean isPlaying() {
        return mState.equals(State.Playing);
    }

    public int getmBufferPosition() {
        return mBufferPosition;
    }

    protected void setmBufferPosition(int position) {
        mBufferPosition = position;
    }

    public void setSong(Audio song) {
        if ( isPlaying()) {
            stopSong();
        }
        sSong = song;
    }

    private void createNotification() {
        new ExecuteNotification().execute();
    }

    class ExecuteNotification extends AsyncTask<Void,Void,Void> {
        private Bitmap logo;

        @Override
        protected Void doInBackground(Void... voids) {
            logo =  BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.ic_cd_music);
            return null;
        }

        @TargetApi(16)
        @Override
        protected void onPostExecute(Void aVoid) {

            Intent playIntent = new Intent(mContext, PlayerService.class);
            playIntent.setAction(ACTION_AUDIO_CONTROLLER);
            playIntent.putExtra(ACTION_AUDIO_CONTROLLER, EXTRA_PLAY_PAUSE);

            PendingIntent pendingPlay = PendingIntent.getService(mContext, 0, playIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            int idButtonPlayOrPause = 0;
            String playOrPause = "";

            if (!isPlaying()) {
                idButtonPlayOrPause = R.drawable.ic_play_arrow_black_24dp;
                playOrPause = "play";
            } else {
                idButtonPlayOrPause = R.drawable.ic_player_pause;
                playOrPause = "pause";
            }

            Notification notification = new Notification.Builder(mContext).
                    setTicker("play song")
                    .setContentText(sSong.getArtist())
                    .setContentTitle(sSong.getTitle())
                    .setSmallIcon(R.drawable.ic_cd_music)
                    .setLargeIcon(logo)
                    .addAction(idButtonPlayOrPause, playOrPause, pendingPlay)
                    .build();

            mNotificationManager.notify(0, notification);
        }
    }

    public boolean isPaused() {
        return mState.equals(State.Paused);
    }

    enum State {
        Retrieving,
        Preparing,
        Playing,
        Paused
    }
}