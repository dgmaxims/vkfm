package com.maximdrobonoh.vkfm.network;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maximdrobonoh.vkfm.model.Profile;

import java.lang.reflect.Type;

/**
 * Created by maximdrobonoh on 16.01.16.
 */
public class ProfileParser implements JsonDeserializer<Profile> {

    @Override
    public Profile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray response = json.getAsJsonObject().get("response").getAsJsonArray();
        JsonObject item = response.get(0).getAsJsonObject();


        Profile profile = new Gson().fromJson(item, Profile.class);
        if ( item.has("city")) {
            JsonObject city = item.get("city").getAsJsonObject();
            String cityTitle =  city.get("title").getAsString();
            profile.city = cityTitle;
        }
        return profile;
    }
}
