package com.maximdrobonoh.vkfm.network;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maximdrobonoh.vkfm.database.DatabaseManager;
import com.maximdrobonoh.vkfm.model.Album;
import com.maximdrobonoh.vkfm.model.AlbumList;
import com.maximdrobonoh.vkfm.model.Artist;
import com.maximdrobonoh.vkfm.model.ArtistList;
import com.maximdrobonoh.vkfm.model.Audio;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.model.Profile;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.ormlite.InDatabaseObjectPersisterFactory;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
    public class VkService extends RetrofitGsonSpiceService {

    private final static String BASE_URL = "https://api.vk.com/method";
    private final static int THREAD_COUNT = 3;

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(VkHttp.class);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        List<Class<?>> classCollection = new ArrayList<>();

        //add persisted classe(s) to class collection
        classCollection.add(Profile.class);
        classCollection.add(Audio.class);
        classCollection.add(AudioList.class);
        classCollection.add(Album.class);
        classCollection.add(AlbumList.class);
        classCollection.add(Artist.class);
        classCollection.add(ArtistList.class);

        InDatabaseObjectPersisterFactory inDatabaseObjectPersisterFactory = new InDatabaseObjectPersisterFactory(
                application, DatabaseManager.getInstance().getHelper(), classCollection);
        cacheManager.addPersister(inDatabaseObjectPersisterFactory);

        return cacheManager;
    }

    @Override
    public int getThreadCount() {
        return THREAD_COUNT;
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(AudioList.class, new AudioParser())
                .registerTypeAdapter(Profile.class, new ProfileParser())
                .registerTypeAdapter(AlbumList.class, new AlbumParser())
                .registerTypeAdapter(ArtistList.class, new ArtistParser())
                .create();

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL);
        return builder;
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}
