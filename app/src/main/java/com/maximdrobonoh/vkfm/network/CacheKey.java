package com.maximdrobonoh.vkfm.network;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public interface CacheKey {
    String POPULAR_MUSIC = "vk_popular_music";
    String POPULAR_SPECIFIC_GENRE = "vk_popular_music_specific_genre";
    String POPULAR_ARTIST = "vk_popular_artist";
    String SEARCHED_MUSIC_BY_ARTIST = "vk_searched_music_by_artist";
    String PROFILE = "vk_profile";
    String ACCOUNT_AUDIO_TRACK = "vk_account_audio_track";
    String ACCOUNT_ALBUM = "vk_account_album";
}
