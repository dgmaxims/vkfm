package com.maximdrobonoh.vkfm.network;

import com.maximdrobonoh.vkfm.model.AlbumList;
import com.maximdrobonoh.vkfm.model.ArtistList;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.model.Profile;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by maximdrobonoh on 05.01.16.
 */
public interface VkHttp {

    String SEARCH_QUERY = "q";
    String AUTO_COMPLETE = "auto_complete";
    String LYRICS  = "lyrics";
    String PERFORMER_ONLY = "performer_only";
    String SORT = "sort";
    String OFFSET = "offset";
    String COUNT = "count";

    @GET("/execute.getPopularArtist")
    ArtistList getPopularArtist(@Query("offset") int offset, @Query("count") int count, @Query("v") String versionApi,
                                @Query("access_token") String token);

    @GET("/execute.getPopularAudioWithAlbum")
    AudioList audioGetPopular(@Query("only_eng") int onlyEng, @Query("genre_id") int genreId,
                               @Query("offset") int offset, @Query("count") int count,
                               @Query("v") String versionApi, @Query("access_token") String accessKey);

    @GET("/execute.searchAudioWithAlbum")
    AudioList searchAudio(@QueryMap Map<String, String> options, @Query("v") String api,
                           @Query("access_token") String accessKey);

    @GET("/execute.getAudioWithAlbums")
    AudioList getAudioWthAlbum(@Query("offset") int offset, @Query("count") int count, @Query("owner_id") long id,
                                @QueryMap Map<String, String> options,
                                @Query("v") String versionApi, @Query("access_token") String token);

    @GET("/execute.getAlbumsWithArt")
    AlbumList getAlbums(@Query("owner_id") long ownerId, @Query("offset") int offset, @Query("count") int count,
                         @Query("v") String versionApi, @Query("access_token") String token);

    @GET("/users.get")
    Profile getProfile(@Query("user_ids")long id, @Query("fields") String fields,
                       @Query("v") String versionApi);
}
