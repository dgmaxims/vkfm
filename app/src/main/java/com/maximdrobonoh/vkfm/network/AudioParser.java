package com.maximdrobonoh.vkfm.network;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.maximdrobonoh.vkfm.model.Audio;
import com.maximdrobonoh.vkfm.model.AudioList;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public class AudioParser implements JsonDeserializer<AudioList> {

    @Override
    public AudioList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
            context) throws JsonParseException {
        Gson parser = new Gson();

        AudioList audioList = new AudioList();
        List<Audio> items = new ArrayList<>();

        JsonArray response = json.getAsJsonObject().get("response").getAsJsonArray();
            for (int i = 0; i < response.size(); i++ ) {
                Audio audio = parser.fromJson(response.getAsJsonArray().get(i), Audio.class);
                items.add(audio);
            };
        audioList.setList(items);
        return audioList;
    }
}
