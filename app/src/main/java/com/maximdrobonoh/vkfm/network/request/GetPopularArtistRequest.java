package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.model.ArtistList;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by maximdrobonoh on 20.01.16.
 */
public class GetPopularArtistRequest extends RetrofitSpiceRequest<ArtistList, VkHttp> {

    private static final String VERSION_API = "5.42";
    private AccessTokenManager tokenManager;

    private int offset;
    private int count;

    public GetPopularArtistRequest(Context context, int offset, int count) {
        super(ArtistList.class, VkHttp.class);

        this.tokenManager = new AccessTokenManager(context);
        this.offset = offset;
        this.count = count;
    }

    @Override
    public ArtistList loadDataFromNetwork() throws Exception {
        ArtistList artistList = getService().getPopularArtist(offset, count, VERSION_API, tokenManager.getAccessKey());
        return artistList;
    }
}
