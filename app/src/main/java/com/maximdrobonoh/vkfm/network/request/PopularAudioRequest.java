package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public class PopularAudioRequest extends RetrofitSpiceRequest<AudioList, VkHttp> {

    private static final String versionApi = "5.42";

    private int onlyEng;
    private int genreId;
    private int offset;
    private int count;

    private AccessTokenManager tokenManager;

    public PopularAudioRequest(Context ctx, int onlyEng, int genreId, int offset, int count) {
        super(AudioList.class, VkHttp.class);
        this.onlyEng = onlyEng;
        this.genreId = genreId;
        this.offset = offset;
        this.count = count;

        tokenManager = new AccessTokenManager(ctx);
    }

    @Override
    public AudioList loadDataFromNetwork() throws Exception {
        final AudioList audios  =   getService().audioGetPopular(onlyEng, genreId, offset, count,
                versionApi, tokenManager.getAccessKey());
        return audios;
    }
}
