package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Map;

/**
 * Created by maximdrobonoh on 16.01.16.
 */
public class SearchAudioRequest extends RetrofitSpiceRequest<AudioList, VkHttp> {

    private static final String API_VERSION = "5.42";
    private Map<String, String> options;
    private AccessTokenManager tokenManager;

    public SearchAudioRequest(Context ctx, Map<String, String> options) {
        super(AudioList.class, VkHttp.class);
        this.options = options;
        this.tokenManager = new AccessTokenManager(ctx);
    }

    @Override
    public AudioList loadDataFromNetwork() throws Exception {
        AudioList audios = getService().searchAudio(options,
                API_VERSION, tokenManager.getAccessKey());
        return audios;
    }
}
