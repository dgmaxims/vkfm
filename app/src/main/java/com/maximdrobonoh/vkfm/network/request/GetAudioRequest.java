package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Hashtable;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
public class GetAudioRequest extends RetrofitSpiceRequest<AudioList, VkHttp> {

    private static final String VERSION_API = "5.42";

    private int offset;
    private int count;
    private AccessTokenManager tokenManager;

    public GetAudioRequest(Context ctx, int offset, int count) {
        super(AudioList.class, VkHttp.class);

        this.tokenManager = new AccessTokenManager(ctx);
        this.offset = offset;
        this.count = count;
    }

    @Override
    public AudioList loadDataFromNetwork() throws Exception {
        AudioList audios = getService().getAudioWthAlbum
                (offset,
                count,
                tokenManager.getUserId(),
                new Hashtable<String, String>(),
                VERSION_API, tokenManager.getAccessKey());
        return audios;
    }
}
