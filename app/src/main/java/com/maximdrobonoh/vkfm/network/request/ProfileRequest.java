package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.model.Profile;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by maximdrobonoh on 16.01.16.
 */
public class ProfileRequest extends RetrofitSpiceRequest<Profile, VkHttp> {

    private final static String API_VERSION = "5.42";
    private AccessTokenManager tokenManager;

    public ProfileRequest(Context ctx){
        super(Profile.class, VkHttp.class);
        tokenManager = new AccessTokenManager(ctx);
    }

    @Override
    public Profile loadDataFromNetwork() throws Exception {
        Long id = tokenManager.getUserId();
        Profile profile = getService().getProfile(id, "photo_50, photo_100", API_VERSION);
        return profile;
    }
}
