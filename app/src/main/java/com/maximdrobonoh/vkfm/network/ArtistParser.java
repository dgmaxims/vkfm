package com.maximdrobonoh.vkfm.network;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.maximdrobonoh.vkfm.model.Artist;
import com.maximdrobonoh.vkfm.model.ArtistList;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 20.01.16.
 */
public class ArtistParser implements JsonDeserializer<ArtistList> {

    @Override
    public ArtistList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray response = json.getAsJsonObject().get("response").getAsJsonArray();
        List<Artist> artists = new ArrayList<>();
        Gson gson = new Gson();

        for ( int i = 0; i < response.size(); i++ ) {
            Artist artist = gson.fromJson(response.get(i), Artist.class);
            artists.add(artist);
        }
        ArtistList artistList = new ArtistList();
        artistList.setList(artists);
        return artistList;
    }
}
