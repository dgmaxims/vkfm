package com.maximdrobonoh.vkfm.network;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.maximdrobonoh.vkfm.model.Album;
import com.maximdrobonoh.vkfm.model.AlbumList;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
public class AlbumParser implements JsonDeserializer<AlbumList> {

    @Override
    public AlbumList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray items = json.getAsJsonObject().get("response").getAsJsonArray();

        Gson gson = new Gson();
        AlbumList albumList = new AlbumList();;

        List<Album> albums = new ArrayList<>();
        for ( int i = 0; i < items.size(); i++ ) {
            Album item = gson.fromJson(items.get(i), Album.class);
            albums.add(item);
        }
        albumList.setList(albums);
        return albumList;
    }
}
