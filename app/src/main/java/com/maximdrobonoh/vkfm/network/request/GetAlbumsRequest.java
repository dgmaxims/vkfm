package com.maximdrobonoh.vkfm.network.request;

import android.content.Context;

import com.maximdrobonoh.vkfm.AccessTokenManager;

import com.maximdrobonoh.vkfm.model.AlbumList;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
public class GetAlbumsRequest extends RetrofitSpiceRequest<AlbumList, VkHttp> {

    private final static String VERSION_API = "5.42";

    private int offset;
    private int count;
    private AccessTokenManager tokenManager;

    public GetAlbumsRequest(Context ctx, int offset, int count) {
        super(AlbumList.class, VkHttp.class);

        this.tokenManager = new AccessTokenManager(ctx);
        this.offset = offset;
        this.count = count;
    }

    @Override
    public AlbumList loadDataFromNetwork() throws Exception {
        AlbumList albums = getService().getAlbums(
                tokenManager.getUserId(),
                offset,
                count,
                VERSION_API,
                tokenManager.getAccessKey());
        return albums;
    }
}
