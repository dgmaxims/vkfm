package com.maximdrobonoh.vkfm.model;

import android.graphics.drawable.Drawable;

/**
 * Created by maximdrobonoh on 12.01.16.
 */
public class Genre {

    private int id;
    private String title;
    private int artRes;

    public Genre(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public Genre(int id, String title, int art) {
        this.id = id;
        this.title = title;
        this.artRes = art;
    }

    public int getArtRes() {
        return artRes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public enum Name {
        ROCK(0), POP(1), RAP_AND_HIPHOP(2), EASY_LISTENING(3), DANCE_AND_HOUSE(4), INSTRUMENTAL(5),
        METAL(6), ALTERNATIVE(21), DUBSTEP(8), JAZZ_BLUES(9), DRUM_AND_BASS(10);

        private final int value;

        Name(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    };

}
