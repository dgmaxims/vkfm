package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximdrobonoh.vkfm.database.Contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by maximdrobonoh on 18.01.16.
 */
@DatabaseTable(tableName = Contract.AUDIO_LIST_TABLE)
public class AudioList {
    @DatabaseField(columnName = BaseColumns._ID,generatedId = true)
    private long id;

    @ForeignCollectionField(eager = false)
    private Collection<Audio> collection;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setList(List<Audio> list) {
        this.collection= list;
    }

    public void setCollection(Collection<Audio> collection) {
        this.collection = collection;
    }

    public Collection<Audio> getCollection() {
        return collection;
    }

    public List<Audio> getList() {
        return new ArrayList<>(collection);
    }
}
