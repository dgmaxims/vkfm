package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximdrobonoh.vkfm.database.Contract;

import com.maximdrobonoh.vkfm.database.Contract.AlbumCollumn;

import java.io.Serializable;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
@DatabaseTable(tableName = Contract.ALBUM_ITEM_TABLE )
public class Album implements Serializable {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    private transient long id;

    @SerializedName("id")
    @DatabaseField(columnName = AlbumCollumn.ALBUM_ID, canBeNull = false, useGetSet = true, dataType = DataType.LONG)
    private long albumId;

    @SerializedName("owner_id")
    @DatabaseField(columnName = AlbumCollumn.OWNER_ID, canBeNull = false, useGetSet = true, dataType = DataType.LONG)
    private long ownerId;

    @SerializedName("title")
    @DatabaseField(columnName = AlbumCollumn.TITLE, canBeNull = false, useGetSet = true, dataType = DataType.STRING)
    private String title;

    @SerializedName("album_50")
    @DatabaseField(columnName = AlbumCollumn.ALBUM_50, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album50;

    @SerializedName("album_100")
    @DatabaseField(columnName = AlbumCollumn.ALBUM_100, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album100;

    @SerializedName("album_200")
    @DatabaseField(columnName = AlbumCollumn.ALBUM_200, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album200;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private AlbumList list;

    public long getAlbumId() {
        return albumId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getAlbum50() {
        return album50;
    }

    public void setAlbum50(String album50) {
        this.album50 = album50;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum200() {
        return album200;
    }

    public void setAlbum200(String album200) {
        this.album200 = album200;
    }

    public String getAlbum100() {
        return album100;
    }

    public void setAlbum100(String album100) {
        this.album100 = album100;
    }

    public AlbumList getList() {
        return list;
    }

    public void setList(AlbumList list) {
        this.list = list;
    }
}
