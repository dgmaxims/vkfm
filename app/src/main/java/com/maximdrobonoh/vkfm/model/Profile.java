package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by maximdrobonoh on 16.01.16.
 */

@DatabaseTable
public class Profile {

    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    public transient int _id;

    @SerializedName("id")
    @DatabaseField
    public int id;

    @SerializedName("first_name")
    @DatabaseField
    public String firstName;

    @SerializedName("last_name")
    @DatabaseField
    public String lastName;

    @SerializedName("photo_50")
    @DatabaseField
    public String photo50;

    @SerializedName("photo_100")
    @DatabaseField
    public String photo100;

    @SerializedName("city")
    @DatabaseField
    public String city;
}
