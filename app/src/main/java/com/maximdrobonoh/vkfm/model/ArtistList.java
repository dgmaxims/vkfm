package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximdrobonoh.vkfm.database.Contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by maximdrobonoh on 20.01.16.
 */
@DatabaseTable(tableName = Contract.ARTIST_LIST_TABLE)
public class ArtistList {

    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private int id;

    @ForeignCollectionField(eager = false)
    private Collection<Artist> collection;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setList(List<Artist> list) {
        this.collection= list;
    }

    public void setCollection(Collection<Artist> collection) {
        this.collection = collection;
    }

    public Collection<Artist> getCollection() {
        return collection;
    }

    public List<Artist> getList() {
        return new ArrayList<>(collection);
    }
}
