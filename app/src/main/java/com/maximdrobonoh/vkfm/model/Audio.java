/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import com.maximdrobonoh.vkfm.database.Contract;
import com.maximdrobonoh.vkfm.database.Contract.AudioColumn;

import java.io.Serializable;
/**
 * An audio object describes an audio file and contains the following fields.
 */
@DatabaseTable(tableName = Contract.AUDIO_ITEM_TABLE)
public class Audio implements Serializable {

    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private transient int id;

    /**
     * Audio ID
     */
    @SerializedName("id")
    @DatabaseField(columnName = AudioColumn.AUDIO_ID, canBeNull = false, useGetSet = true, dataType = DataType.LONG)
    private long audioId;

    /**
     * Audio owner ID
     */
    @SerializedName("owner_id")
    @DatabaseField(columnName = AudioColumn.OWNER_ID, canBeNull = false, useGetSet = true, dataType = DataType.LONG)
    private long ownerId;

    /**
     * Artist name
     */
    @SerializedName("artist")
    @DatabaseField(columnName = AudioColumn.ARTIST, canBeNull = false, useGetSet = true, dataType = DataType.STRING)
    private String artist;

    /**
     * Audio file title
     */
    @SerializedName("title")
    @DatabaseField(columnName = AudioColumn.TITILE, canBeNull = false, useGetSet = true, dataType = DataType.STRING)
    private String title;

    /**
     * Duration(in seconds)
     */
    @SerializedName("duration")
    @DatabaseField(columnName = AudioColumn.DURATION, canBeNull = false, useGetSet = true, dataType = DataType.LONG)
    private long duration;

    /**
     * Link to mp3
     */
    @SerializedName("url")
    @DatabaseField(columnName = AudioColumn.DOWNLOAD_URL, canBeNull = false, useGetSet = true, dataType = DataType.STRING)
    private String url;

    /**
     * ID of the lyrics(if available) of the audio file
     */
    @SerializedName("lyrics_id")
    @DatabaseField(columnName = AudioColumn.LYRICS_ID ,canBeNull =true, useGetSet = true, dataType = DataType.LONG)
    private long lyricsId;

    /**
     * ID of the album containing the audio file
     */
    @SerializedName("album_id")
    @DatabaseField(columnName = AudioColumn.ALBUM_ID ,canBeNull =true, useGetSet = true, dataType = DataType.LONG)
    private long albumId;

    /**
     * Genre ID.
     */
    @SerializedName("genre_id")
    @DatabaseField(columnName = AudioColumn.GENRE_ID, canBeNull = false, useGetSet = true, dataType = DataType.INTEGER)
    private int genreId;

    /**
     * Audio album url 50
     */
    @SerializedName("album_50")
    @DatabaseField(columnName = AudioColumn.ART_URL_50, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album50;

    /**
     * Audio album url 100
     */
    @SerializedName("album_100")
    @DatabaseField(columnName = AudioColumn.ART_URL_100, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album100;

    /**
     * Audio album url 200
     */
    @SerializedName("album_200")
    @DatabaseField(columnName = AudioColumn.ART_URL_200, canBeNull = true, useGetSet = true, dataType = DataType.STRING)
    private String album200;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private AudioList list;

    public Audio() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getLyricsId() {
        return lyricsId;
    }

    public void setLyricsId(long lyricsId) {
        this.lyricsId = lyricsId;
    }

    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getAlbum50() {
        return album50;
    }

    public void setAlbum50(String album50) {
        this.album50 = album50;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getAlbum100() {
        return album100;
    }

    public void setAlbum100(String album100) {
        this.album100 = album100;
    }

    public String getAlbum200() {
        return album200;
    }

    public void setAlbum200(String album200) {
        this.album200 = album200;
    }

    public AudioList getList() {
        return list;
    }

    public void setList(AudioList list) {
        this.list = list;
    }


    public long getAudioId() {
        return audioId;
    }

    public void setAudioId(long audioId) {
        this.audioId = audioId;
    }
}
