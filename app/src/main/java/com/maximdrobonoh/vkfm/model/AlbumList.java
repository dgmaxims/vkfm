package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximdrobonoh.vkfm.database.Contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by maximdrobonoh on 18.01.16.
 */
@DatabaseTable(tableName = Contract.ALBUM_LIST_TABLE)
public class AlbumList {
    @DatabaseField(columnName = BaseColumns._ID,generatedId = true)
    private long id;

    @ForeignCollectionField(eager = false)
    private Collection<Album> collection;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setList(List<Album> list) {
        this.collection= list;
    }

    public void setCollection(Collection<Album> collection) {
        this.collection = collection;
    }

    public Collection<Album> getCollection() {
        return collection;
    }

    public List<Album> getList() {
        return new ArrayList<>(collection);
    }
}
