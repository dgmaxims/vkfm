package com.maximdrobonoh.vkfm.model;

import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import com.maximdrobonoh.vkfm.database.Contract;
import com.maximdrobonoh.vkfm.database.Contract.ArtistCollumn;

import java.io.Serializable;

/**
 * Created by maximdrobonoh on 20.01.16.
 */

@DatabaseTable(tableName = Contract.ARTIST_ITEM_TABLE)
public class Artist implements Serializable {
    @DatabaseField(generatedId = true, columnName = BaseColumns._ID)
    private transient long _id;

    @SerializedName("owner_id")
    @DatabaseField(columnName = ArtistCollumn.OWNER_ID, dataType = DataType.LONG, canBeNull = false, useGetSet = true)
    private long ownerId;

    @SerializedName("name")
    @DatabaseField(columnName = ArtistCollumn.NAME, dataType = DataType.STRING, canBeNull = false, useGetSet = true)
    private String name;

    @SerializedName("art_50")
    @DatabaseField(columnName = ArtistCollumn.ART_50, dataType = DataType.STRING, canBeNull = true, useGetSet = true)
    private String art50;

    @SerializedName("art_100")
    @DatabaseField(columnName = ArtistCollumn.ART_100, dataType = DataType.STRING, canBeNull = true, useGetSet = true)
    private String art100;

    @SerializedName("art_200")
    @DatabaseField(columnName = ArtistCollumn.ART_200, dataType = DataType.STRING, canBeNull = true, useGetSet = true)
    private String art200;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private ArtistList list;

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArt50() {
        return art50;
    }

    public void setArt50(String art50) {
        this.art50 = art50;
    }

    public String getArt200() {
        return art200;
    }

    public void setArt200(String art200) {
        this.art200 = art200;
    }

    public String getArt100() {
        return art100;
    }

    public void setArt100(String art100) {
        this.art100 = art100;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public ArtistList getList() {
        return list;
    }

    public void setList(ArtistList list) {
        this.list = list;
    }
}
