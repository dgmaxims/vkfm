package com.maximdrobonoh.vkfm.database;

import android.content.Context;

import com.maximdrobonoh.vkfm.model.Audio;
import com.maximdrobonoh.vkfm.database.Contract.AudioColumn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 18.01.16.
 */
public class DatabaseManager {

    private static DatabaseManager instance;

    static public void init(Context ctx) {
        if ( instance == null ) {
            instance = new DatabaseManager(ctx);
        }
    }

    static public DatabaseManager getInstance() {
        return instance;
    }

    private DatabaseHelper helper;
    private DatabaseManager(Context ctx) {
        helper = new DatabaseHelper(ctx);
    }

    public DatabaseHelper getHelper() {
        return helper;
    }

    public List<Audio> getAllAudios() {
        List<Audio> audioList = new ArrayList<>();
        try {
            audioList.addAll(getHelper().getAudioItemDao().queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return audioList;
    }

    public List<Audio> getAudiosByOwnerIdAndDistinctArtist(long ownerId) {
        List<Audio> audios = new ArrayList<>();
        try {
            audios.addAll(getHelper().getAudioItemDao()
                    .queryBuilder()
                    .distinct()
                    .selectColumns(AudioColumn.ART_URL_100, AudioColumn.ART_URL_200, AudioColumn.ARTIST)
                    .where().in(AudioColumn.OWNER_ID, ownerId)
                    .query());
        } catch ( SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return audios;
    }

    public List<Audio> getAudiosDistinctArtist() {
        List<Audio> audios = new ArrayList<>();
        try {
            audios.addAll(getHelper().getAudioItemDao()
                    .queryBuilder()
                    .distinct()
                    .selectColumns(AudioColumn.ART_URL_100, AudioColumn.ART_URL_200, AudioColumn.ARTIST)
                    .query());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return audios;
    }
}
