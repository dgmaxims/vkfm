package com.maximdrobonoh.vkfm.database;

/**
 * Created by maximdrobonoh on 18.01.16.
 */
public class Contract {

    public static final String AUDIO_ITEM_TABLE = "audio_item";
    public static final String AUDIO_LIST_TABLE = "audio_list";

    public static final String ALBUM_ITEM_TABLE ="album_item";
    public static final String ALBUM_LIST_TABLE = "album_list";

    public static final String ARTIST_ITEM_TABLE = "artist_item";
    public static final String ARTIST_LIST_TABLE = "artist_list_table";

    public interface AudioColumn {
        String AUDIO_ID = "audio_id";
        String OWNER_ID = "owner_id";
        String ARTIST = "artist";
        String TITILE = "title";
        String DURATION = "dutation";
        String DOWNLOAD_URL = "url";
        String LYRICS_ID = "lyrics_id";
        String ALBUM_ID = "album_id";
        String GENRE_ID = "genre_id";
        String ART_URL_50 = "art_url_50";
        String ART_URL_100 = "art_url_100";
        String ART_URL_200 = "art_url_200";
    }

    public interface AlbumCollumn {
        String ALBUM_ID = "album_id";
        String OWNER_ID = "owner_id";
        String TITLE = "tite";
        String ALBUM_50 = "album_50";
        String ALBUM_100 = "album_100";
        String ALBUM_200 = "album_200";
    }

    public interface ArtistCollumn {
        String OWNER_ID = "owner_id";
        String NAME = "name";
        String ART_50 = "art_url_50";
        String ART_100 = "art_url_100";
        String ART_200 = "art_url_200";
    }
}
