package com.maximdrobonoh.vkfm.database;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.maximdrobonoh.vkfm.model.Audio;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.octo.android.robospice.persistence.ormlite.RoboSpiceDatabaseHelper;

import java.sql.SQLException;

/**
 * Created by maximdrobonoh on 18.01.16.
 */
public class DatabaseHelper extends RoboSpiceDatabaseHelper {

    private static final String DATABASE_NAME ="vk.db";
    private static final int VERSION = 1;

    private Dao<AudioList, Integer> audioListDao = null;
    private Dao<Audio, Integer> audioItemDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, VERSION);
    }

    public Dao<AudioList, Integer> getAudioListDao() {
        if ( audioListDao == null ) {
            try {
                audioListDao = getDao(AudioList.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return audioListDao;
    }

    public Dao<Audio, Integer> getAudioItemDao () {
        if ( audioItemDao == null ) {
            try {
                audioItemDao = getDao(Audio.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return audioItemDao;
    }
}
