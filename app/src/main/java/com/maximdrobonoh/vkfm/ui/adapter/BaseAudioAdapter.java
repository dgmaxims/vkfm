package com.maximdrobonoh.vkfm.ui.adapter;

import android.animation.Animator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.marshalchen.ultimaterecyclerview.animators.internal.ViewHelper;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Audio;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 16.01.16.
 */
public abstract class BaseAudioAdapter  extends UltimateViewAdapter<BaseAudioAdapter.ViewHolder> {

    List<Audio> audios;
    Context context;

    protected int mDuration = 300;
    protected Interpolator mInterpolator = new LinearInterpolator();
    protected int mLastPosition = 5;
    protected boolean isFirstOnly = true;

    public BaseAudioAdapter(Context context, List<Audio> audios) {
        this.audios = audios;
        this.context = context;
    }

    public List<Audio> getAll() {
        return audios;
    }

    public Audio getAudo(int position) {
        return audios.get(position);
    }

    public void add(Audio audio) {
        this.audios.add(audio);
        notifyDataSetChanged();
    }

    public void addAll(List<Audio> audios) {
        this.audios.addAll(audios);
        notifyDataSetChanged();
    }

    public void cleanAudios() {
        audios.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return audios.size();
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_audio, parent,
                false);;
        return new ViewHolder(v);
    }

    @Override
    public int getAdapterItemCount() {
        return audios.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    public void insert(Audio audio, int position) {
        insert(audios, audio, position);
    }

    public void remove(int position) {
        remove(audios, position);
    }

    public void clear() {
        clear(audios);
    }

    public void swapPositions(int from, int to) {
        swapPositions(audios, from, to);
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new RecyclerView.ViewHolder(new View(parent.getContext())){};
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        swapPositions(fromPosition,toPosition);
        super.onItemMove(fromPosition, toPosition);
    }

    public void setOnDragStartListener(OnStartDragListener startDragListener) {
        mDragStartListener =startDragListener;
    }

    protected void toAnimate(int position,ViewHolder holder) {
        if (!isFirstOnly || position > mLastPosition) {
            for (Animator anim : getAdapterAnimations(holder.itemView, AdapterAnimationType.ScaleIn)) {
                anim.setDuration(mDuration).start();
                anim.setInterpolator(mInterpolator);
            }
            mLastPosition = position;
        } else {
            ViewHelper.clear(holder.itemView);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @Bind(R.id.image_play)
        ImageView playImageView;
        @Nullable
        @Bind(R.id.audio_artist)
        TextView artistTextView;
        @Nullable
        @Bind(R.id.audio_title)
        TextView titleTextView;
        @Nullable
        @Bind(R.id.audio_duration)
        TextView durationTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
