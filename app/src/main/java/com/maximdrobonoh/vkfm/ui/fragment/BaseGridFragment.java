package com.maximdrobonoh.vkfm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.RecyclerItemClickListener;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.uiUtils.BasicGridLayoutManager;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.ui.activity.ArtistTracksActivity;
import com.maximdrobonoh.vkfm.ui.adapter.ListArtistAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 20.01.16.
 */
public abstract class BaseGridFragment extends BaseFragment {

    @Bind(R.id.recycler_view)
    UltimateRecyclerView recyclerView;

    protected ListArtistAdapter adapter;
    protected static final int NEED_LOAD_ARTISTS = 20;
    private BasicGridLayoutManager layoutManager;
    private int columns = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_grid, container, false);
        ButterKnife.bind(this, view);

        adapter = new ListArtistAdapter(getActivity());
        layoutManager = new BasicGridLayoutManager(getActivity(), columns, adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(getLoadMoreListener());
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        startArtistTrackActivity(position);
                    }
                }));
        recyclerView.setAdapter(adapter);
        return view;
    }


    protected abstract UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener();

    protected void showRequestFailedMessage() {
        Toast.makeText(getActivity(), "failure update songs", Toast.LENGTH_SHORT)
                .show();
    }

    protected abstract void executeRequest();

    private void startArtistTrackActivity(int position) {
        Intent showTracksIntent = new Intent(getActivity(), ArtistTracksActivity.class);
        showTracksIntent.putExtra(ArtistTracksActivity.EXTRA_ARTIST,
                adapter.getItem(position).getName());
        showTracksIntent.putExtra(ArtistTracksActivity.EXTRA_ART_URL,
                adapter.getItem(position).getArt200());
        startActivity(showTracksIntent);
    }
}
