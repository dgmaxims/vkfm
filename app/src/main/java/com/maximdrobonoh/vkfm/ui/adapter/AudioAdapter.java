package com.maximdrobonoh.vkfm.ui.adapter;

import android.content.Context;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Audio;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public class AudioAdapter extends BaseAudioAdapter {

    public AudioAdapter(Context context, List<Audio> audios) {
        super(context, audios);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (audios.size() > 0  && holder.titleTextView != null ) {
            Audio audio = audios.get(position);

            holder.titleTextView.setText(audio.getTitle());
            holder.artistTextView.setText(audio.getArtist());
            holder.durationTextView.setText(audio.getDuration() / 60 + ":" +
                    (audio.getDuration() % 60 == 0 ? "00" : audio.getDuration() % 60));

            if ( String.valueOf(audio.getDuration() % 60).length() == 1 )
                holder.durationTextView.setText(holder.durationTextView.getText() + "0");

            Picasso.with(context).load(audio.getAlbum100() == null ? audio.getAlbum50(): audio.getAlbum100())
                    .error(R.drawable.ic_default_album)
                    .into(holder.playImageView);
        }
        toAnimate(position, holder);
    }
}
