package com.maximdrobonoh.vkfm.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.Util;
import com.maximdrobonoh.vkfm.model.AccessToken;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Activity for authorization by OAuth2.0
 *
 * This activity is used to authorize new user or continue
 * work with app if user authorized
 *
 * @author MaximDrobonoh
 * @version 2016.0105
 * @since 1.0
 */
public class LoginActivity extends AppCompatActivity {

    //auth
    private static final String REDIRECT_URI = "https://oauth.vk.com/blank.html";
    private static final String BASE_URI = "https://oauth.vk.com/authorize?";
    private static final String ERROR = "error";
    private static final String CANCEL = "cancel";

    @Bind(R.id.login_web_view)
    WebView webView;

    Context context;
    AccessTokenManager tokenManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = this;
        tokenManager = new AccessTokenManager(this);

        if (tokenManager.getAccessKey().equals(AccessTokenManager.ACCESS_TOKEN_NULL)) {
            loadPage();
        } else {
            startAllGenresActivity();
        }
    }

    private void startAllGenresActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    private void loadPage() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new OAuthWebViewClient(context));

        StringBuilder urlRequest = new StringBuilder();
        urlRequest.append(BASE_URI);

        for (Map.Entry<String, String> param : OAuthParameters.getOAuthParametes().entrySet()) {
            urlRequest.append(param.getKey() + "=" + param.getValue() + "&");
        }
        urlRequest.deleteCharAt(urlRequest.length() - 1);
        webView.loadUrl(urlRequest.toString());
    }

    private class OAuthWebViewClient extends WebViewClient {
        boolean canShowPage = true;
        Context context;

        public OAuthWebViewClient(Context ctx) {
            context = ctx;
        }

        boolean processUrl(String url) {
            if (url.startsWith(REDIRECT_URI)) {
                String extraData = url.substring(url.indexOf('#') + 1);

                Map<String, String> result = Util.explodeQueryString(extraData);

                if (result.containsKey(ERROR) || result.containsKey(CANCEL)) {
                    //TODO show alter dialog about error
                } else {
                    storeParameters(result);
                    startAllGenresActivity();
                }
                canShowPage = false;
                return true;
            }
            return false;
        }

        void storeParameters(Map<String, String> params) {
            AccessToken accessToken = new AccessToken();
            accessToken.key = params.get(AccessTokenManager.ACCESS_TOKEN);
            accessToken.expiresIn = Integer.parseInt(params.get(AccessTokenManager.EXPIRES_IN));
            accessToken.userId = params.get(AccessTokenManager.USER_ID);

            tokenManager.saveAccessToken(accessToken);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (processUrl(url))
                return true;
            canShowPage = true;
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            processUrl(url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (canShowPage) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            canShowPage = false;

            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext())
                    .setMessage(R.string.alert_dialog_network_error)
                    .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            loadPage();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //TODO exit from app
                        }
                    });
            builder.show();
        }
    }

    private static class OAuthParameters {

        private static final String CLIENT_ID = "client_id";
        private static final String REDIRECT_URL = "redirect_uri";
        private static final String DISPLAY = "display";
        private static final String SCOPE = "scope";
        private static final String RESPONSE_TYPE = "response_type";
        private static final String API = "v";
        private static final String REVOKE = "revoke";

        private final static Map<String, String> oAuthParameters = new HashMap<String, String>() {
            {
                put(CLIENT_ID, "5215713");
                put(REDIRECT_URL, "https://oauth.vk.com/blank.html");
                put(DISPLAY, "mobile");
                put(SCOPE, "audio,offline");
                put(RESPONSE_TYPE, "token");
                put(API, "5.28");
                put(REVOKE, "1");
            }
        };
        public static Map<String, String> getOAuthParametes() {
            return oAuthParameters;
        }
    }
}
