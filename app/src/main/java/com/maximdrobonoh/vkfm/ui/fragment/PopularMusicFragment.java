package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.request.PopularAudioRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public class PopularMusicFragment extends BaseMusicFragment {

    private static final String EXTRA_GENRE_ID = "com.maximdrobonoh.vkfm.PopularMusicFragment.genreId";
    private static final String EXTRA_CACHE_KEY = "com.maximdrobonoh.vkfm.PopularMusicFragment.cacheKey";

    private int genreId = 0;
    private String cacheKey ;
    private int offset = 0;

    public static PopularMusicFragment newInstnance(int genreId, String cacheKey) {
        PopularMusicFragment fragment = new PopularMusicFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_GENRE_ID, genreId);
        args.putString(EXTRA_CACHE_KEY, cacheKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.genreId = getArguments().getInt(EXTRA_GENRE_ID, 1);
        this.cacheKey = getArguments().getString(EXTRA_CACHE_KEY);
        this.offset = 0;

        executeRequest();
    }

    @Override
    public void onStop() {
        super.onStop();
        offset = 0;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void executeRequest() {
        spiceManager.execute(getPopularAudioRequest(offset,genreId),
                cacheKey, DurationInMillis.ONE_HOUR, listPopularAudioRequestListener);
    }

    @Override
    protected UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener() {
        return new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                if ( maxLastVisiblePosition == adapter.getItemCount()-1) {
                    offset +=NEED_LOAD_TRACKS;
                    cacheKey += offset;
                    executeRequest();
                }
            }
        };
    }

    private PopularAudioRequest getPopularAudioRequest(int offset, int genreId ) {
        int onlyEng = 0;
        return new PopularAudioRequest(getContext(),onlyEng, genreId,
                offset, NEED_LOAD_TRACKS );
    }

    private ListPopularAudioRequestListener listPopularAudioRequestListener =
            new ListPopularAudioRequestListener();

    private final class ListPopularAudioRequestListener implements RequestListener<AudioList> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getActivity(), "failure update songs", Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        public void onRequestSuccess(AudioList audios) {
            adapter.addAll(audios.getList());
        }
    }
}
