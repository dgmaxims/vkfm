package com.maximdrobonoh.vkfm.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.uiUtils.BasicGridLayoutManager;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Genre;
import com.maximdrobonoh.vkfm.ui.activity.GenreTracksActivity;
import com.maximdrobonoh.vkfm.ui.adapter.GenreGridAdapter;
import com.maximdrobonoh.vkfm.ui.adapter.RecyclerItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 12.01.16.
 */
public class GenreFragment extends Fragment {

    @Bind(R.id.recycler_view)
    UltimateRecyclerView recyclerView;

    private GenreGridAdapter adapter;
    private BasicGridLayoutManager layoutManager;
    private int columns = 2;

    public static GenreFragment newInstance() {
        GenreFragment fragment = new GenreFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_grid,  container, false);
        ButterKnife.bind(this, view);

        adapter = new GenreGridAdapter(getActivity());
        layoutManager = new BasicGridLayoutManager(getActivity(),columns, adapter);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Genre clickedGenre = adapter.getGenre(position);
                        Intent showTracksIntent = new Intent(getActivity(), GenreTracksActivity.class);
                        showTracksIntent.putExtra(GenreTracksActivity.EXTRA_GENRE_ID, clickedGenre.getId());
                        showTracksIntent.putExtra(GenreTracksActivity.EXTRA_TITLE, clickedGenre.getTitle());
                        showTracksIntent.putExtra(GenreTracksActivity.EXTRA_ART_RES_ID, clickedGenre.getArtRes());
                        startActivity(showTracksIntent);

                    }
                }));
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
