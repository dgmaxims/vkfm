package com.maximdrobonoh.vkfm.ui.asyncLoader;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.maximdrobonoh.vkfm.AccessTokenManager;
import com.maximdrobonoh.vkfm.database.DatabaseManager;
import com.maximdrobonoh.vkfm.model.Audio;

import java.util.List;

/**
 * Created by maximdrobonoh on 19.01.16.
 */
public class AudioDistictAccountArtistLoader extends AsyncTaskLoader<List<Audio>> {

    private AccessTokenManager tokenManager;

    public AudioDistictAccountArtistLoader(Context context) {
        super(context);
        tokenManager = new AccessTokenManager(context);
    }

    @Override
    public List<Audio> loadInBackground() {
        return DatabaseManager.getInstance().getAudiosByOwnerIdAndDistinctArtist(tokenManager.getUserId());
    }
}
