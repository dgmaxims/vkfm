package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.request.GetAudioRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
public class AccountTrackFragment extends BaseMusicFragment {

    private static final String ARG_CACHE_KEY = AccountTrackFragment.class.getSimpleName() + ".cacheKey";

    private GetAudiosListener getAudiosListener = new GetAudiosListener();
    private int offset = 0;
    private String cacheKey;

    public static AccountTrackFragment getInstance(String cacheKey) {
        AccountTrackFragment fragment = new AccountTrackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CACHE_KEY, cacheKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener() {
        return new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                if ( maxLastVisiblePosition == adapter.getItemCount()-1) {
                    offset +=NEED_LOAD_TRACKS;
                    cacheKey += offset;
                    executeRequest();
                }
            }
        };
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.cacheKey = getArguments().getString(ARG_CACHE_KEY);
        executeRequest();
    }

    @Override
    protected void executeRequest() {
        spiceManager.execute(getAudioRequest(), cacheKey, DurationInMillis.ONE_HOUR,
                getAudiosListener);
    }

    private GetAudioRequest getAudioRequest()  {
        GetAudioRequest request = new GetAudioRequest(getActivity(), offset, NEED_LOAD_TRACKS);
        return  request;
    }

    private class GetAudiosListener implements RequestListener<AudioList> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getActivity(), "failure update songs", Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        public void onRequestSuccess(AudioList audios) {
            adapter.addAll(audios.getList());
        }
    }
}
