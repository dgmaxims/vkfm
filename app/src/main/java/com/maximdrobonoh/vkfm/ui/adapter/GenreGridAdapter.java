package com.maximdrobonoh.vkfm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Genre;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by maximdrobonoh on 12.01.16.
 */
public class GenreGridAdapter extends GridAdapter {

    final private List<Genre> genreList = new ArrayList<>();

    private Context context;

    public GenreGridAdapter(Context ctx) {
        genreList.add(new Genre(1, ctx.getString(R.string.Rock)));
        genreList.add(new Genre(2, ctx.getString(R.string.Pop)));
        genreList.add(new Genre(3, ctx.getString(R.string.RapAndHipHop)));
        genreList.add(new Genre(4, ctx.getString(R.string.EasyListening, R.drawable.easy_listening_logo)));
        genreList.add(new Genre(5, ctx.getString(R.string.DanceAndHouse)));
        genreList.add(new Genre(6, ctx.getString(R.string.Instumental)));
        genreList.add(new Genre(7, ctx.getString(R.string.Metal)));
        genreList.add(new Genre(21, ctx.getString(R.string.Alternative)));
        genreList.add(new Genre(8, ctx.getString(R.string.DubStep)));
        genreList.add(new Genre(9, ctx.getString(R.string.JazzAndBlues)));
        genreList.add(new Genre(10, ctx.getString(R.string.DrumAndBass)));
        genreList.add(new Genre(11, ctx.getString(R.string.Trance)));
        genreList.add(new Genre(12, ctx.getString(R.string.Chanson)));

        this.context = ctx;
    }

    public Genre getGenre(int position) {
        return genreList.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_card_view, parent, false);
        return new HolderGirdCell(view, true);
    }

    @Override
    public int getAdapterItemCount() {
        return genreList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ( VIEW_TYPES.HEADER == getItemViewType(position)) {
            onBindHeaderViewHolder(holder, position);
        } else if (VIEW_TYPES.NORMAL == getItemViewType(position)) {
            Genre genre = genreList.get(position);

            HolderGirdCell girdCell = (HolderGirdCell)holder;
            girdCell.title.setText(genre.getTitle());

            if ( genre.getArtRes() != 0 ) {
                Picasso.with(context).load(genre.getArtRes())
                        .into(((HolderGirdCell) holder).art);
            }
        }
    }
}
