package com.maximdrobonoh.vkfm.ui.fragment;

import android.support.v4.app.Fragment;

import com.maximdrobonoh.vkfm.network.VkService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by maximdrobonoh on 06.01.16.
 */
public class BaseFragment extends Fragment {
    private SpiceManager spiceManager = new SpiceManager(VkService.class);

    @Override
    public void onStart() {
        spiceManager.start(getContext());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
