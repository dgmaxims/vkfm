package com.maximdrobonoh.vkfm.ui.activity;

import android.support.v4.app.Fragment;
import android.widget.LinearLayout;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.network.CacheKey;
import com.maximdrobonoh.vkfm.ui.fragment.ArtistMusicFragment;
import com.squareup.picasso.Picasso;

/**
 * Created by maximdrobonoh on 15.01.16.
 */
public class ArtistTracksActivity extends BaseTracksActivity {

    public final static String EXTRA_ARTIST = ArtistTracksActivity.class.getSimpleName() + ".artist";;
    public final static String EXTRA_ART_URL = ArtistTracksActivity.class.getSimpleName() + ".art";

    @Override
    protected void initFragment() {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setId(R.id.linearFragmentContainer);

        String artist = getIntent().getStringExtra(EXTRA_ARTIST);
        String cacheKey = CacheKey.SEARCHED_MUSIC_BY_ARTIST +
                artist;
        String url = getIntent().getStringExtra(EXTRA_ART_URL);

        Picasso.with(this).load(url).into(artCover);

        Fragment fragment = ArtistMusicFragment.getInstance(artist,cacheKey);
        getSupportFragmentManager().beginTransaction().add(ll.getId(),fragment).commit();
        fragmentContainer.addView(ll);
    }
}
