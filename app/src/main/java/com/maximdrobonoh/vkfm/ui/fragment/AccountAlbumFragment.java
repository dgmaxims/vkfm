package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.RecyclerItemClickListener;
import com.marshalchen.ultimaterecyclerview.uiUtils.BasicGridLayoutManager;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.AlbumList;
import com.maximdrobonoh.vkfm.network.CacheKey;
import com.maximdrobonoh.vkfm.network.request.GetAlbumsRequest;
import com.maximdrobonoh.vkfm.ui.adapter.ListAlbumAdapter;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 17.01.16.
 */
public class AccountAlbumFragment extends BaseFragment {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private static final int NEED_TO_LOAD = 10;
    private int offset = 0;
    private String cacheKey;
    private ListAlbumAdapter adapter;
    private BasicGridLayoutManager layoutManager;
    private int columns = 2;

    public static AccountAlbumFragment getInstance() {
        return new AccountAlbumFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.fragment_grid, container, false);
        ButterKnife.bind(this, view);

        adapter = new ListAlbumAdapter(getContext());
        layoutManager = new BasicGridLayoutManager(getActivity(), columns, adapter);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        //TODO on item click. Load songs of selected album
                    }
                }));
        //TODO load by offset
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cacheKey = CacheKey.ACCOUNT_ALBUM;
        executeRequest();
    }

    protected void executeRequest() {
        getSpiceManager().execute(getAlbumsRequest(), cacheKey,
                DurationInMillis.ONE_HOUR, new ListAudioDistinctArtist());
    }

    private GetAlbumsRequest getAlbumsRequest() {
        return new GetAlbumsRequest(getActivity(), offset, NEED_TO_LOAD);
    }

    private class ListAudioDistinctArtist implements RequestListener<AlbumList> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getActivity(), "failure update songs", Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        public void onRequestSuccess(AlbumList albumList) {
            adapter.addAll(albumList.getList());
        }
    }
}
