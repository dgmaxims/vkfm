package com.maximdrobonoh.vkfm.ui.asyncLoader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.maximdrobonoh.vkfm.database.DatabaseManager;
import com.maximdrobonoh.vkfm.model.Audio;

import java.util.List;

/**
 * Created by maximdrobonoh on 14.01.16.
 */
public class AudioDistinctPopularArtistLoader extends AsyncTaskLoader<List<Audio>> {

    private List<Audio> audios;

    public AudioDistinctPopularArtistLoader(Context context) {
        super(context);
    }

    @Override
    public List<Audio> loadInBackground() {
         return DatabaseManager.getInstance().getAudiosDistinctArtist();
    }

    @Override
    public void deliverResult(List<Audio> data) {
        audios = data;
        if (isStarted()) {
            super.deliverResult(data);
       }
    }

    @Override
    protected void onStartLoading() {
        if (audios != null) {
            deliverResult(audios);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStartLoading();
    }
}
