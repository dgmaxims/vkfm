package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Audio;
import com.maximdrobonoh.vkfm.network.VkService;
import com.maximdrobonoh.vkfm.player.PlayerService;
import com.maximdrobonoh.vkfm.ui.adapter.AudioAdapter;
import com.maximdrobonoh.vkfm.ui.adapter.RecyclerItemClickListener;
import com.octo.android.robospice.SpiceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 15.01.16.
 */
public abstract class BaseMusicFragment extends Fragment  {

    @Bind(R.id.popular_music_recycler_view)
    UltimateRecyclerView recyclerView;
    @Bind(R.id.selected_audio_title)
    TextView selectedAudioTitle;
    @Bind(R.id.selected_audio_artist)
    TextView selectedAudioArtist;
    @Bind(R.id.selected_audio_art)
    ImageView selectedAudioArt;
    @Bind(R.id.player_control)
    ImageView playerControl;

    protected SpiceManager spiceManager = new SpiceManager(VkService.class);
    protected AudioAdapter adapter;
    protected static final int NEED_LOAD_TRACKS = 20;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_popular_music, container, false);
        ButterKnife.bind(this, v);
        adapter = new AudioAdapter(getContext(), new ArrayList<Audio>());
        initRecyclerView();;
        playerControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playOrPause();
            }
        });
        resumeControlBar();
        return v;
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initRecyclerView() {
        final LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(getLoadMoreListener());
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Audio audio = adapter.getAudo(position);
                        setAudioControlBar(audio);

                        PlayerService.getInstance().stopSong();
                        PlayerService.getInstance().setSong(audio);
                        PlayerService.getInstance().toPlay();

                        playerControl.setImageResource(R.drawable.ic_player_pause);
                    }
                }));
        adapter.setCustomLoadMoreView(LayoutInflater.from(getActivity()).inflate(
                R.layout.bottom_progress_bar, null));
        recyclerView.setAdapter(adapter);
        }

    protected UltimateRecyclerView getRecyclerView() {
        return recyclerView;
    }

    protected abstract UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener();
    protected abstract void  executeRequest();

    protected void playOrPause() {
        if (! PlayerService.getInstance().isPaused() ) {
            PlayerService.getInstance().pauseSong();
            playerControl.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        } else {
            PlayerService.getInstance().continueToPlay();
            playerControl.setImageResource(R.drawable.ic_player_pause);
        }
    }

    private void resumeControlBar() {
        if ( PlayerService.getInstance().isPlaying()) {
            playerControl.setImageResource(R.drawable.ic_player_pause);
            setAudioControlBar(PlayerService.getInstance().getCurrentSong());
        }
    }

    protected void setAudioControlBar(Audio audio) {
        selectedAudioTitle.setText(audio.getTitle());
        selectedAudioArtist.setText(audio.getArtist());
        Picasso.with(getActivity()).load(audio.getAlbum100()== null ? audio.getAlbum50(): audio.getAlbum100())
                .error(R.drawable.ic_default_album)
                .into(selectedAudioArt);
    }
}
