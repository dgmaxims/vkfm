package com.maximdrobonoh.vkfm.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.maximdrobonoh.vkfm.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by maximdrobonoh on 15.01.16.
 */
public  abstract class BaseTracksActivity extends AppCompatActivity {

    public static final String EXTRA_TITLE = "com.maximdrobonoh.vkfm.extraTitle";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.image)
    ImageView artCover;
    @Bind(R.id.llFragmentContainer)
    LinearLayout fragmentContainer;

    private static final String EXTRA_IMAGE = "com.maximdrobonoh.vkfm.extraImage";

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        setContentView(R.layout.activity_tracks);
        ButterKnife.bind(this);

        initCollapsingToolbarLayout();
        initFragment();
    }
    protected abstract void initFragment();

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void initCollapsingToolbarLayout() {
        ViewCompat.setTransitionName(ButterKnife.findById(this, R.id.app_bar_layout), EXTRA_IMAGE);
        supportPostponeEnterTransition();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getStringExtra(EXTRA_TITLE);

        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));
        collapsingToolbarLayout.setTitle(title);
        artCover.setImageResource(R.drawable.ic_rock_logo);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (NullPointerException e) {
            return false;
        }
    }
}
