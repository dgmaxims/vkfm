package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.maximdrobonoh.vkfm.model.ArtistList;
import com.maximdrobonoh.vkfm.network.CacheKey;
import com.maximdrobonoh.vkfm.network.request.GetPopularArtistRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by maximdrobonoh on 14.01.16.
 */
public class PopularArtistFragment extends BaseGridFragment {

    int offset;
    String cacheKey;

    public static PopularArtistFragment getInstance() {
        PopularArtistFragment fragment = new PopularArtistFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        offset = 0;
        cacheKey = CacheKey.POPULAR_ARTIST;
        executeRequest();
    }

    @Override
    protected void executeRequest() {
        getSpiceManager().execute(getPopularArtistRequest(offset), cacheKey, DurationInMillis.ONE_HOUR,  new PopularArtistRequestLintener());
    }

    @Override
    protected UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener() {
        return new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                    offset += NEED_LOAD_ARTISTS;
                    cacheKey += offset;
                    executeRequest();

            }
        };
    }

    private GetPopularArtistRequest getPopularArtistRequest(int offset) {
        return new GetPopularArtistRequest(getContext(), offset, NEED_LOAD_ARTISTS);
    }

    private class PopularArtistRequestLintener implements RequestListener<ArtistList> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            showRequestFailedMessage();
        }

        @Override
        public void onRequestSuccess(ArtistList artistList) {
            adapter.addAll(artistList.getList());
        }
    }
}