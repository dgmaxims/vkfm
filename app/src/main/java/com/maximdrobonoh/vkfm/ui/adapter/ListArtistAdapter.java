package com.maximdrobonoh.vkfm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.maximdrobonoh.vkfm.model.Artist;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 14.01.16.
 */
public class ListArtistAdapter extends GridAdapter {

    private Context context;
    private List<Artist> audios = new ArrayList<>();

    public ListArtistAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<Artist> audios) {
        this.audios.addAll(audios);
        notifyDataSetChanged();
    }

    public void clear() {
        audios.clear();
        notifyDataSetChanged();
    }

    public Artist getItem(int position) {
        return audios.get(position);
    }

    @Override
    public int getAdapterItemCount() {
        return audios.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ( VIEW_TYPES.HEADER == getItemViewType(position)) {
            onBindHeaderViewHolder(holder, position);
        } else if (VIEW_TYPES.NORMAL == getItemViewType(position)) {
            Artist artist = getItem(position);

            HolderGirdCell girdCell = (HolderGirdCell)holder;
            girdCell.title.setText(artist.getName());
            Picasso.with(context)
                    .load(artist.getArt100() == null ? artist.getArt50() : artist.getArt100())
                    .into(girdCell.art);
        }
    }
}
