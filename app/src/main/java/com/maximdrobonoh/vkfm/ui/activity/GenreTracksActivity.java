package com.maximdrobonoh.vkfm.ui.activity;

import android.support.v4.app.Fragment;
import android.widget.LinearLayout;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.model.Genre;
import com.maximdrobonoh.vkfm.network.CacheKey;
import com.maximdrobonoh.vkfm.ui.fragment.PopularMusicFragment;
import com.squareup.picasso.Picasso;


public class GenreTracksActivity extends BaseTracksActivity  {

    public static final String EXTRA_GENRE_ID = GenreTracksActivity.class.getSimpleName()
            + ".genreId";
    public static final String EXTRA_ART_RES_ID = GenreTracksActivity.class.getSimpleName()
            + ",art";

    @Override
    protected void initFragment() {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setId(R.id.linearFragmentContainer);

        int genreId = getIntent().getIntExtra(EXTRA_GENRE_ID,
                Genre.Name.ALTERNATIVE.getValue());
        String cacheKey = CacheKey.POPULAR_SPECIFIC_GENRE +
                genreId;

        Fragment fragment = PopularMusicFragment.newInstnance(genreId, cacheKey);
        getSupportFragmentManager().beginTransaction().add(ll.getId(),fragment).commit();

        fragmentContainer.addView(ll);
    }
}
