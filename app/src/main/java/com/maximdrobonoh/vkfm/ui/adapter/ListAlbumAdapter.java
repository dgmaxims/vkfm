package com.maximdrobonoh.vkfm.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.maximdrobonoh.vkfm.model.Album;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 19.01.16.
 */
public class ListAlbumAdapter extends GridAdapter {

    private List<Album> items = new ArrayList<>();
    private Context context;

    public ListAlbumAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<Album> newAlbums) {
        this.items.addAll(newAlbums);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public Album getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getAdapterItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ( VIEW_TYPES.HEADER == getItemViewType(position)) {
            onBindHeaderViewHolder(holder, position);
        } else if (VIEW_TYPES.NORMAL == getItemViewType(position)) {
           Album album = getItem(position);

            HolderGirdCell girdCell = (HolderGirdCell)holder;
            girdCell.title.setText(album.getTitle());
            Picasso.with(context)
                    .load(album.getAlbum100() == null ? album.getAlbum50() : album.getAlbum100())
                    .into(girdCell.art);
        }
    }
}
