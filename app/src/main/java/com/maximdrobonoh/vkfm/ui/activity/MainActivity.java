package com.maximdrobonoh.vkfm.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.maximdrobonoh.vkfm.R;
import com.maximdrobonoh.vkfm.Util;
import com.maximdrobonoh.vkfm.model.Profile;
import com.maximdrobonoh.vkfm.network.CacheKey;
import com.maximdrobonoh.vkfm.network.request.ProfileRequest;
import com.maximdrobonoh.vkfm.network.VkService;
import com.maximdrobonoh.vkfm.ui.fragment.AccountMusicTabs;
import com.maximdrobonoh.vkfm.ui.fragment.PopularMusicTabs;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SpiceManager spiceManager = new SpiceManager(VkService.class);

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.fab)
    FloatingActionButton fab;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    @Bind(R.id.nav_view)
    NavigationView navigationView;

    TextView headerTitle;
    TextView headerSubtitile;
    ImageView headerAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        View view  = navigationView.inflateHeaderView(R.layout.nav_header_main);
        headerTitle = ButterKnife.findById(view, R.id.nav_header_title);
        headerSubtitile = ButterKnife.findById(view, R.id.nav_header_subtitle);
        headerAvatar = ButterKnife.findById(view, R.id.nav_header_imageView);
        ButterKnife.findById(view, R.id.nav_header_subtitle);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        spiceManager.execute(new ProfileRequest(this), CacheKey.PROFILE, DurationInMillis.ALWAYS_RETURNED,
                new GetProfileLInstener());
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_music) {
            toolbar.setTitle("My music");
            startAccountMusicActivity();
        } else if (id == R.id.nav_popular) {
            toolbar.setTitle("Popular");
            startPopularMusicActivity();

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_help) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startPopularMusicActivity() {
        getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_main, new PopularMusicTabs())
                .commit();
    }

    private void startAccountMusicActivity() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_main, new AccountMusicTabs())
                .commit();
    }

    private class GetProfileLInstener implements RequestListener<Profile> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(Profile profile) {
            headerTitle.setText(profile.firstName + " " + profile.lastName);
            if ( profile.city != null ) {
                headerSubtitile.setText(profile.city);
            }
            Picasso.with(MainActivity.this).load(profile.photo100)
            .transform(new Util.CircleTransform()).into(headerAvatar);
        }
    }
}
