package com.maximdrobonoh.vkfm.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.maximdrobonoh.vkfm.model.AudioList;
import com.maximdrobonoh.vkfm.network.request.SearchAudioRequest;
import com.maximdrobonoh.vkfm.network.VkHttp;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by maximdrobonoh on 15.01.16.
 */
public class ArtistMusicFragment extends BaseMusicFragment  {

    private static final int SORT_BY_POPULAR = 2;
    private static final String ARG_ARTIST = ArtistMusicFragment.class.getSimpleName() + ".artist";
    private static final String ARG_CACHE_KEY = ArtistMusicFragment.class.getSimpleName() + ".cache_key";

    private int offset = 0;
    private String artist;
    private String cacheKey;
    private SearchAudioRequestListener requestListener = new SearchAudioRequestListener();

    public static ArtistMusicFragment getInstance(String artist, String cacheKey) {
        ArtistMusicFragment fragment = new ArtistMusicFragment();
        Bundle args = new Bundle();

        args.putString(ARG_ARTIST, artist);
        args.putString(ARG_CACHE_KEY, cacheKey);
        fragment.setArguments(args);

        return  fragment;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.artist = getArguments().getString(ARG_ARTIST);
        this.cacheKey = getArguments().getString(ARG_CACHE_KEY);

        executeRequest();
    }

    @Override
    protected UltimateRecyclerView.OnLoadMoreListener getLoadMoreListener() {
        return new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                if ( maxLastVisiblePosition == adapter.getItemCount()-1) {
                    offset +=NEED_LOAD_TRACKS;
                    cacheKey += offset;
                    executeRequest();
                }
            }
        };
    }

    @Override
    protected void executeRequest() {
        spiceManager.execute(getSearchAudioRequest(), cacheKey, DurationInMillis.ONE_HOUR,
                requestListener);
    }

    private SearchAudioRequest getSearchAudioRequest() {
        Map<String, String> options = new Hashtable<>();
        options.put(VkHttp.COUNT, String.valueOf(NEED_LOAD_TRACKS));
        options.put(VkHttp.OFFSET, String.valueOf(offset));
        options.put(VkHttp.SORT, String.valueOf(SORT_BY_POPULAR));
        options.put(VkHttp.SEARCH_QUERY, artist);

        SearchAudioRequest request = new SearchAudioRequest(getActivity(), options);
        return request;
    }

    private class SearchAudioRequestListener implements RequestListener<AudioList> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getActivity(), "failure update songs", Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        public void onRequestSuccess(AudioList audios) {
            Toast.makeText(getActivity(), "updated", Toast.LENGTH_SHORT)
                    .show();
            adapter.addAll(audios.getList());
        }
    }
}
